Release 8.x-1.0
Issues resolved:
#2830784: The editor filter is sticking to the comment text area
#2830785: Node page comment section below buttons need theming
#2830788: Space between send and preview buttons in contact page breaking.
#2831007: Pager on Bootstrap blog D8 theme doesn't look like actual theme
#2831006: Change in styling of logo and post links
#2830790: When secondary menu is present the top menu breaks
#2830794: search results need gaps like main page
#2830796: Link on logo is missing
#2831001: Update normalise to 4.2.0 in Bootstrap for D8
#2830782: Space needed between main column and right column.
#2830869: Aileron fonts inside font directory
#2830760: Screenshot of the theme need to be updated
#2830758: Bootstrap blog D8 port
#2829608: Release update file for Bootstrap Blog for D8
