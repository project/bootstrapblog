/**
 * @file
 * script.js
 *
 * Provides general enhancements and fixes to Bootstrap Blog's JS files.
 */

var Drupal = Drupal || {};

(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.bootstrapblog = {
    attach: function (context) {
      // Enhancements.
    }
  };
})(jQuery, Drupal);
